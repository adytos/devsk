<?php 

 /* Template name: Domov */

get_header();

$hero = get_field("hero");
$about = get_field("about");
$team = get_field("team");
$gallery = get_field("gallery");
$contact = get_field("contact");
?>


<section class="hero" id="home">
    <div class="container">
        <h1 class="hero-title">
            Hey there, we are Spacecowboy
        </h1>
        <p class="hero-text">
            - An independent space agency -
        </p>
        <div class="hero-btn">
            <a href=""> Learn more </a>
        </div>
    </div>
</section>

<section class="about" id="about">
    <div class="container">
        <?php if(!empty($about['nadpis'])){ ?>
        <h2 class="about-title">
            <?php echo $about['nadpis']; ?>
        </h2>
        <?php }?>
        <?php if(!empty($about['podnadpis'])){ ?>
        <div class="about-text">
            <?php echo $about['podnadpis']; ?>
        </div>
        <?php }?>
        <div class="about-box__wrap">
          <?php while ( have_rows( 'about' ) ) : the_row();
                 while ( have_rows( 'item' ) ) : the_row(); ?>
                <div class="about-box">
                <?php
                  $ikona = get_sub_field('ikona');

                  if (!empty($ikona)) :
                  ?>
                  <div class="about-box__img">
                      <img src="<?php echo $ikona['url']; ?>" alt="">
                  </div>
                  <?php endif; ?>
                  <h3>
                      <?php the_sub_field('hlavny_nadpis'); ?>
                  </h3>
                  <p>
                      <?php the_sub_field('popis'); ?>
                  </p>
                 </div>
               <?php  endwhile;
             endwhile; 
             ?>


        </div>
    </div>
</section>
<section class="team" id="team">
    <div class="container">
        <?php if(!empty($team['nadpis'])){ ?>
        <h2 class="team-title">
            <?php echo $team['nadpis']; ?>
        </h2>
        <?php }?>
        <?php if(!empty($team['podnadpis'])){ ?>
        <div class="team-text">
            <?php echo $team['podnadpis']; ?>
        </div>
        <?php }?>
    </div>
</section>
<section class="gallery" id="immpresions">
    <div class="container">
        <?php if(!empty($gallery['nadpis'])){ ?>
        <h2 class="gallery-title">
            <?php echo $gallery['nadpis']; ?>
        </h2>
        <?php }?>
        <?php if(!empty($gallery['podnadpis'])){ ?>
        <div class="gallery-text">
            <?php echo $gallery['podnadpis']; ?>
        </div>
        <?php }?>
        <div class="gallery-item__wrap">
            <?php if(!empty(($gallery['galeria']))){ 
          $images = $gallery['galeria'];
              if ($images) : 

          foreach ($images as $image) : ?>
            <div class="gallery-item">
                <a href="<?php echo esc_url($image['url']); ?>" data-fancybox="gallery">
                    <img loading="lazy" src="<?php echo esc_url($image['sizes']['large']); ?>"
                        alt="<?php echo esc_attr($image['alt']); ?>">
                </a>
            </div>
            <?php endforeach; 

          endif; 

        }?>
        </div>
    </div>
</section>
<section class="contact" id="contact">
    <div class="container">
        <?php if(!empty($contact['nadpis'])){ ?>
        <h2 class="contact-title">
            <?php echo $contact['nadpis']; ?>
        </h2>
        <?php }?>
        <?php if(!empty($contact['podnadpis'])){ ?>
        <div class="contact-text">
            <?php echo $contact['podnadpis']; ?>
        </div>
        <?php }?>
        <div class="contact-form__inner">
            <?php echo do_shortcode('[contact-form-7 id="5" title="Contact form 1"]')?>
        </div>
    </div>
</section>






<?php get_footer();?>