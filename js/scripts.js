$(document).ready(function () {  

  $(window).scroll(function ()
  {
      if ($(window).scrollTop() > 300)
      {
          if (!$("header").hasClass("scrolled")) $("header").addClass("scrolled");
      }
      else
      {
          $("header").removeClass("scrolled");
              
      }
  });
  $(window).scroll();


   $(".menu-toggle").on("click", function (e) {
    e.preventDefault();
    $(".main-navigation").toggleClass("menu-opened");
    $("body").toggleClass("toggled-view");
   });  

   $(".primary-menu a").on("click", function(){
    $("body").removeClass("toggled-view");
   })
   
   
 
   $('a[href^="#"]').on('click', function(event) {
    event.preventDefault();
    
    var target = $(this.getAttribute('href'));
    
    if (target.length) {
      $('html, body').stop().animate({
        scrollTop: target.offset().top -30
      }, 800);
    }
  }); 
 

});


