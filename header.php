<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php bloginfo('description');?></title>
	<link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	



	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

    <header class="header">
            <div class="container">
                <div class="header-main">
                    <div class="header-main__logo">
                        <a href="<?php echo get_home_url(); ?>"><img  loading="lazy" src="<?php echo get_field('logo', 'option')['url'];?>" alt="<?php echo get_field('logo', 'option')['alt'];?>"> </a>
                    </div>
                    <div class="header-main__menu">
                        <?php
                            wp_nav_menu(
                                array(
                                    'theme_location' => 'primary_menu',
                                    'menu_id'        => 'primary-menu',
                                    'menu_class'     => 'primary-menu'
                                )
                            );
                        ?>
                       

                        <div class="menu-toggle">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>
                    </div>
                </div>
            </div>       
     </header>