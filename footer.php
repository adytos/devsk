<footer class="footer">
    <div class="container">
        <p class="footer-copyright"> <?php the_field('copyright', 'option');?>  </p>
    </div>
</footer>


<?php wp_footer(); ?>


</body>
</html>
